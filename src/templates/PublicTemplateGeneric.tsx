import Head from 'next/head';
import React from 'react';
import { FC } from 'react';

interface Props {
	title?: string;
	description?: string;
	children: React.ReactChild | React.ReactNode;
}
const PublicTemplateGeneric: FC<Props> = ({ title, description, children }) => {
	return (
		<>
			<Head>
				{title && <title>{title}</title>}
				{description && (
					<meta
						key="description"
						content={description}
						name="description"
					/>
				)}
			</Head>
			{children}
			{/* any services here */}
		</>
	);
};

export default PublicTemplateGeneric;
