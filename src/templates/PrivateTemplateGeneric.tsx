import Head from 'next/head';
import React, { useContext } from 'react';
import { AuthContext } from '../core/auth';

interface Props {
	title?: string;
	description?: string;
	children: React.ReactChild | React.ReactNode;
}
const PrivateTemplateGeneric = ({ title, description, children }: Props) => {
	const { auth, loading } = useContext(AuthContext);

	return (
		<>
			<Head>
				{title && <title>{title}</title>}
				{description && (
					<meta
						key="description"
						content={description}
						name="description"
					/>
				)}
			</Head>
			{!auth && loading ? (
				<div>loading component here...</div>
			) : !auth ? (
				<div>Auth page component here</div>
			) : (
				<>{children}</>
			)}
			{/* any services here */}
		</>
	);
};

export default PrivateTemplateGeneric;
