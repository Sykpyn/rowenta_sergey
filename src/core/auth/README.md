# The auth context and a wrapper

Yout can use it, wrapped \_app.tsx in the next way:

```tsx
<AuthWrap>
	<Component {...pageProps} />
</AuthWrap>
```

And then use methods inside other components like:

```tsx
const { auth, loading } = useContext(AuthContext);
```
