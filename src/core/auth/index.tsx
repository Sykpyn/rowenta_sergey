import React, { useEffect, useState } from 'react';

// define any user object here or other type
type authType = null | {};
export type AuthContextType = {
	auth: authType;
	setAuth: (state: authType) => void;
	loading: boolean;
	setLoading: (state: boolean) => void;
};
export const AuthContext = React.createContext(
	(null as unknown) as AuthContextType
);

const AuthWrap = ({ children }) => {
	const [auth, setAuth] = useState(null);
	const [loading, setLoading] = useState(true);
	// useEffect(() => {
	// 	fetchAuth();
	// }, []);

	// const fetchAuth = () => {
	// 	setTimeout(async () => {
	// 		setLoading(true);
	// 		// some code for the fetch user data
	// 		try {
	// 			const data = await something();
	// 			await setAuth(data);
	// 		} catch (error) {
	// 			//   Make error
	// 			await setAuth(null);
	// 			await setLoading(false);
	// 		}
	// 	}, 1);
	// };

	return (
		<AuthContext.Provider value={{ auth, setAuth, loading, setLoading }}>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthWrap;
