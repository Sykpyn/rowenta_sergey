import { createContext, useState } from 'react';

type globalVariableType = null | string;
export type StoreContextType = {
	globalVariable: globalVariableType;
	setGlobalVariable: (state: globalVariableType) => void;
};
export const StoreContext = createContext<StoreContextType>(
	(null as unknown) as StoreContextType
);

export const StoreContextWrap = ({ children }) => {
	const [globalVariable, setGlobalVariable] = useState<null | string>(null);

	return (
		<StoreContext.Provider value={{ globalVariable, setGlobalVariable }}>
			{children}
		</StoreContext.Provider>
	);
};
