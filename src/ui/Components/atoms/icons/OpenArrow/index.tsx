import React from 'react';

const OpenArrow = () => (
	<svg
		width="21"
		height="12"
		viewBox="0 0 21 12"
		fill="none"
		xmlns="http://www.w3.org/2000/svg"
	>
		<path d="M10.5 0L0.540707 12L20.4593 12L10.5 0Z" fill="#757575" />
	</svg>
);

export default OpenArrow;
