import React from 'react';
import styles from './style.module.css';

const Corners = () => (
	<>
		<svg
			width="29"
			height="24"
			viewBox="0 0 29 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={styles.left_top}
		>
			<path d="M28.5 0.5H0.5V24" stroke="white" stroke-miterlimit="10" />
		</svg>
		<svg
			width="29"
			height="24"
			viewBox="0 0 29 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={styles.right_top}
		>
			<path d="M28.5 24V0.5H0.5" stroke="white" stroke-miterlimit="10" />
		</svg>
		<svg
			width="29"
			height="21"
			viewBox="0 0 29 21"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={styles.right_bottom}
		>
			<path d="M0.5 20.5H28.5V0" stroke="white" stroke-miterlimit="10" />
		</svg>
		<svg
			width="29"
			height="21"
			viewBox="0 0 29 21"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			className={styles.left_bottom}
		>
			<path d="M0.5 0V20.5H28.5" stroke="white" stroke-miterlimit="10" />
		</svg>
	</>
);

export default Corners;
