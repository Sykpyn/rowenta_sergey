import React from 'react';

const OkSign = () => (
	<svg
		width="23"
		height="23"
		viewBox="0 0 23 23"
		fill="none"
		xmlns="http://www.w3.org/2000/svg"
	>
		<g clip-path="url(#clip0)">
			<path
				d="M11.5 -0.00012207C5.152 -0.00012207 0 5.15188 0 11.4999C0 17.8479 5.152 22.9999 11.5 22.9999C17.848 22.9999 23 17.8479 23 11.4999C23 5.15188 17.848 -0.00012207 11.5 -0.00012207ZM10.2 16.2499L5.45 11.4999L7 9.99988L10.2 12.9954L16.9285 6.26688L18.55 7.89988L10.2 16.2499Z"
				fill="#797979"
			/>
		</g>
		<defs>
			<clipPath id="clip0">
				<rect width="23" height="23" fill="white" />
			</clipPath>
		</defs>
	</svg>
);

export default OkSign;
