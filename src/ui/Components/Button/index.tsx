import React, { FC } from 'react';
import styles from './style.module.css';
import LeftArrow from '../atoms/icons/LeftArrow';
import RightArrow from '../atoms/icons/RightArrow';

interface Props {
	text: string;
	backgroundColor?: string;
	arrowDirection?: 'left' | 'right';
	type?: string;
}

const Button: FC<Props> = ({
	text = 'example',
	backgroundColor = '#CC0000',
	arrowDirection = 'left',
	type,
}) => (
	<div
		className={styles.buttom_main}
		style={{ backgroundColor: backgroundColor }}
	>
		<div className={styles.content}>
			<div className={styles.arrow}>
				{arrowDirection === 'left' ? <LeftArrow /> : <RightArrow />}
			</div>
			<div>{text}</div>
		</div>
	</div>
);

export default Button;
