import React from 'react';
import styles from './style.module.css';

const Footer = () => (
	<div className={styles.footer_main}>
		<div className={styles.footer_chat}></div>
	</div>
);

export default Footer;
