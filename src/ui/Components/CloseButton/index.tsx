import React from 'react';

const CloseButton = () => (
	<svg
		width="34"
		height="34"
		viewBox="0 0 34 34"
		fill="none"
		xmlns="http://www.w3.org/2000/svg"
	>
		<path d="M34 0H0V34H34V0Z" fill="#797979" />
		<path
			fill-rule="evenodd"
			clip-rule="evenodd"
			d="M14.8821 18L10.766 13.8839L12.5338 12.1161L16.6499 16.2322L20.766 12.1161L22.5338 13.8839L18.4176 18L22.5338 22.1161L20.766 23.8839L16.6499 19.7677L12.5338 23.8839L10.766 22.1161L14.8821 18Z"
			fill="white"
		/>
	</svg>
);

export default CloseButton;
