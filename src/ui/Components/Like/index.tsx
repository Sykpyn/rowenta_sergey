import React, { FC, useState } from 'react';
import styles from './style.module.css';
import FullLike from './FullLike';
import EmptyLike from './EmptyLike';

interface Props {
	count: number;
	isLiked?: boolean;
}

const Like: FC<Props> = ({ count, isLiked = true }) => {
	return (
		<div className={styles.like_block}>
			{isLiked ? <FullLike /> : <EmptyLike />}
			<div className={styles.count}>{count}</div>
		</div>
	);
};

export default Like;
