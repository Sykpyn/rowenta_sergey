import React, { ChangeEvent, FC, useState } from 'react';
import { FieldError } from 'react-hook-form';
import ErrorSign from '../atoms/icons/ErrorSign';
import OkSign from '../atoms/icons/OkSign';
import styles from './styles.module.css';

interface Props {
	refTo?: any;
	type: string;
	placeholder: string;
	name: string;
	error: FieldError | undefined;
}

const TextInput: FC<Props> = ({ refTo, type, placeholder, name, error }) => {
	const [value, setValue] = useState<boolean>(false);

	const onChange = (event: ChangeEvent<HTMLInputElement>) => {
		event.currentTarget.value ? setValue(true) : setValue(false);
	};

	return (
		<div className={styles.input_main}>
			<input
				className={
					error
						? styles.error
						: value
						? styles.input
						: styles.clean_input
				}
				ref={refTo}
				type={type}
				placeholder={placeholder}
				name={name}
				onChange={onChange}
			></input>
			{error && <p className={styles.error_message}>{error.message}</p>}
			<div className={styles.sign}>
				{error ? <ErrorSign /> : value ? <OkSign /> : null}
			</div>
		</div>
	);
};

export default TextInput;
