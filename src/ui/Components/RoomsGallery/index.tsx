import React, { FC } from 'react';
import Room from './OneRoom';
import styles from './style.module.css';

interface Props {
	roomsAmount?: number;
}

const RoomsGallery: FC<Props> = ({ roomsAmount = 15 }) => {
	return (
		<div className={styles.rooms_gallery_main}>
			{[...Array(roomsAmount)].map((x, i) => (
				<Room key={i} number={++i} />
			))}
		</div>
	);
};

export default RoomsGallery;
