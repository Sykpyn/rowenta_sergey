import React, { FC } from 'react';
import styles from '../style.module.css';
import Link from 'next/link';
import Corners from '../../atoms/icons/Corners';

interface Props {
	number: number;
}

const Room: FC<Props> = ({ number }) => {
	return (
		<Link href={`/room/${number}`}>
			<div className={styles.room_main}>
				<Corners />
				<p>кімната {number}</p>
			</div>
		</Link>
	);
};

export default Room;
