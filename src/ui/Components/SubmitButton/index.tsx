import React, { FC } from 'react';
import styles from './styles.module.css';
import LeftArrow from '../atoms/icons/LeftArrow';
import RightArrow from '../atoms/icons/RightArrow';
import { string } from 'yup/lib/locale';

interface Props {
	text: string;
	backgroundColor?: string;
	arrowDirection?: 'left' | 'right';
	type?: any;
}

const SubmitButton: FC<Props> = ({
	text = 'зареєструватися',
	backgroundColor = '#CC0000',
	arrowDirection = 'left',
	...props
}) => (
	<button
		{...props}
		className={styles.buttom_main}
		style={{ backgroundColor: backgroundColor }}
	>
		{' '}
		<div className={styles.content}>
			<div className={styles.arrow}>
				{arrowDirection === 'left' ? <LeftArrow /> : <RightArrow />}
			</div>
			<div>{text}</div>
		</div>
	</button>
);

export default SubmitButton;
