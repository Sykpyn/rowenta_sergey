import React, { FC, useState } from 'react';
import styles from './style.module.css';
import Dropdown, { Option } from 'react-dropdown';
import OpenArrow from '../atoms/icons/OpenArrow';
import CloseArrow from '../atoms/icons/CloseArrow';
import { FieldError } from 'react-hook-form';

interface Props {
	data: Array<any>;
	name: string;
	refTo: any;
	error: FieldError | undefined;
}

const Select: FC<Props> = ({ data, name, refTo, error }) => {
	const [chosenValue, setChosenValue] = useState<Option>(undefined);

	return (
		<div className={styles.select_block}>
			<input
				type="text"
				name={name}
				ref={refTo}
				style={{ display: 'none' }}
			/>
			<Dropdown
				options={data}
				className={styles.root}
				value={'Оберіть команду'}
				onChange={(e) => setChosenValue(e)}
				placeholderClassName={
					chosenValue ? styles.placeholder_select : styles.placeholder
				}
				menuClassName={styles.menu}
				controlClassName={styles.control}
				arrowOpen={<OpenArrow />}
				arrowClosed={<CloseArrow />}
			/>
			{error && !chosenValue && (
				<span className={styles.error}>Это поля обязательное</span>
			)}
		</div>
	);
};

export default Select;
