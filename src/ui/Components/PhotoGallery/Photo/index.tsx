import React, { FC, useState } from 'react';
import styles from '../styles.module.css';
import Image from 'next/image';
import Like from '../../Like';

interface Props {
	imageUrl: string;
	likeCount: number;
	isLiked: boolean;
	onOpenModal: any;
}

const Photo: FC<Props> = ({ imageUrl, likeCount, isLiked, onOpenModal }) => {
	const [count, setCount] = useState(likeCount);
	const [like, setLike] = useState(isLiked);

	const handleClick = () => {
		if (like === true) {
			setCount(likeCount);
			setLike(false);
			return;
		}
		setCount(likeCount + 1);
		setLike(true);
	};

	return (
		<div className={styles.photo_main}>
			<span onClick={() => onOpenModal(imageUrl)}>
				<Image src={imageUrl} width={278} height={210} />
			</span>
			<span onClick={() => handleClick()}>
				<Like count={count} isLiked={like} />
			</span>
		</div>
	);
};

export default Photo;
