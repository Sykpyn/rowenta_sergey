import Link from 'next/link';
import React, { FC, useState } from 'react';
import Button from '../Button';
import Photo from './Photo';
import styles from './styles.module.css';
import Image from 'next/image';

import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import CloseButton from '../CloseButton';

const Data = [
	{ imgUrl: '/img/photo1.png', likeCount: 3, isLiked: false },
	{ imgUrl: '/img/photo2.png', likeCount: 6, isLiked: false },
	{ imgUrl: '/img/photo3.png', likeCount: 9, isLiked: true },
	{ imgUrl: '/img/photo4.png', likeCount: 12, isLiked: false },
	{ imgUrl: '/img/photo5.png', likeCount: 15, isLiked: false },
	{ imgUrl: '/img/photo6.png', likeCount: 18, isLiked: false },
	{ imgUrl: '/img/photo7.png', likeCount: 21, isLiked: false },
	{ imgUrl: '/img/photo8.png', likeCount: 24, isLiked: false },
	{ imgUrl: '/img/photo9.png', likeCount: 27, isLiked: false },
	{ imgUrl: '/img/photo10.png', likeCount: 30, isLiked: false },
	{ imgUrl: '/img/photo11.png', likeCount: 33, isLiked: true },
	{ imgUrl: '/img/photo12.png', likeCount: 36, isLiked: false },
	{ imgUrl: '/img/photo13.png', likeCount: 39, isLiked: false },
	{ imgUrl: '/img/photo14.png', likeCount: 42, isLiked: false },
	{ imgUrl: '/img/photo15.png', likeCount: 45, isLiked: false },
];

const PhotoGallery = () => {
	const [open, setOpen] = useState(false);

	const onOpenModal = (currentImageUrl) => {
		setImgUrl(currentImageUrl);
		setOpen(true);
	};
	const onCloseModal = () => setOpen(false);

	const [imgUrl, setImgUrl] = useState('/img/videoMock.png');

	return (
		<>
			<div className={styles.button_group}>
				<div className={styles.one_button}>
					<Link href={'/'}>
						<a>
							<Button
								text={'на головну'}
								backgroundColor={'#4D8CAB'}
								arrowDirection={'left'}
							/>
						</a>
					</Link>
				</div>
				<div className={styles.one_button}>
					<Link href={'./register'}>
						<a>
							<Button
								text={'реєстрація'}
								backgroundColor={'#576423'}
								arrowDirection={'right'}
							/>
						</a>
					</Link>
				</div>
				<div className={styles.one_button}>
					<Button
						text={'рейтинг'}
						backgroundColor={'#F7922A'}
						arrowDirection={'right'}
					/>
				</div>
				<div className={styles.one_button}>
					<Link href={'./rooms'}>
						<a>
							<Button
								text={'відеокімната'}
								backgroundColor={'#46316E'}
								arrowDirection={'right'}
							/>
						</a>
					</Link>
				</div>
			</div>
			<div className={styles.title_block}>
				<h1 className={styles.title}>голосування</h1>
			</div>
			<div className={styles.photos_main}>
				{Data.map((el) => (
					<Photo
						imageUrl={el.imgUrl}
						likeCount={el.likeCount}
						isLiked={el.isLiked}
						onOpenModal={onOpenModal}
					/>
				))}
			</div>
			<Modal
				open={open}
				onClose={onCloseModal}
				center
				classNames={{
					modal: styles.modal_main,
					overlay: styles.overlay,
				}}
				showCloseIcon={false}
			>
				<div className={styles.modal_text}>
					<Image src={imgUrl} width={1210} height={680}></Image>
					<div
						className={styles.close_btn}
						onClick={() => onCloseModal()}
					>
						<CloseButton />
					</div>
				</div>
			</Modal>
		</>
	);
};

export default PhotoGallery;
