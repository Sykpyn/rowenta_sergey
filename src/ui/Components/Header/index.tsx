import React from 'react';
import styles from './style.module.css';
import Image from 'next/image';

const Header = () => (
	<div className={styles.header_main}>
		<div className={styles.header_wrapper}>
			<div className={styles.logo}>
				<Image src="/img/logo.png" alt="logo" width={75} height={176} />
			</div>
			<div className={styles.header_title}>
				<span className={styles.red_title}>Better </span>
				<span className={styles.grey_title}>Living</span>
			</div>
		</div>
	</div>
);

export default Header;
