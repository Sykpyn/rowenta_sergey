import React, { FC } from 'react';
import { useRouter } from 'next/router';
import MainLayout from '../../layouts/MainLayout';
import styles from './styles.module.css';
import Image from 'next/image';
import Button from '../../ui/Components/Button';
import Link from 'next/link';

const Room = () => {
	const router = useRouter();
	return (
		<MainLayout>
			<div className={styles.go_to_main_button}>
				<Link href="/">
					<a>
						<Button text={'На головну'} />
					</a>
				</Link>
			</div>
			<div className={styles.title_block}>
				<h1 className={styles.title}>
					кімната {router.query.roomNumber}
				</h1>
			</div>
			<div className={styles.video}>
				<Image
					src="/img/videoMock.png"
					alt="logo"
					width={1391}
					height={768}
				/>
			</div>
		</MainLayout>
	);
};

export default Room;
