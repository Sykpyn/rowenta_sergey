// global styles here
import '../ui/Styles/bace/global.css';
import '../ui/Styles/abstract/colors.css';
import '../ui/Styles/abstract/fonts.css';

function MyApp({ Component, pageProps }) {
	return <Component {...pageProps} />;
}

export default MyApp;
