# Storyblock service

```zsh
yarn add storyblok-js-client storyblok-react
```

Use it inside template:

```tsx
import StoryblokInstance from '..../services/storyblock';

...

{StoryblokInstance.bridge()}
```

And inside the component:

```tsx
import StoryblokInstance from '..../services/storyblock';
import SbEditable from 'storyblok-react';


const SampleComponent = ({res}) => {
    const { content } = res.data.story;

    ...

    <SbEditable content={content}>
        ...
    </SbEditable>
}

Component.getInitialProps = async ({ _, query }) => {
	StoryblokInstance.setQuery(query);

	let res = await StoryblokInstance.get('cdn/stories/url_page', {});

	return {
		res,
	};
};
```
