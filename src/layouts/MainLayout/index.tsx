import React, { FC } from 'react';
import styles from './style.module.css';
import Header from '../../ui/Components/Header';

type Props = {
	children: React.ReactNode;
};

const MainLayout: FC<Props> = ({ children }) => {
	return (
		<div className={styles.main_layout}>
			<Header />
			<div className={styles.main_wrapper}>{children}</div>
		</div>
	);
};

export default MainLayout;
