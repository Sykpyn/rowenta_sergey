import React from 'react';
import MainLayout from '../../../layouts/MainLayout';
import Button from '../../../ui/Components/Button';
import styles from './styles.module.css';
import Link from 'next/link';
import Image from 'next/image';

const Main = () => (
	<MainLayout>
		<div className={styles.button_group}>
			<div className={styles.one_button}>
				<Link href={'./voting'}>
					<a>
						<Button
							text={'голосування'}
							backgroundColor={'#4D8CAB'}
							arrowDirection={'right'}
						/>
					</a>
				</Link>
			</div>
			<div className={styles.one_button}>
				<Link href={'./register'}>
					<a>
						<Button
							text={'реєстрація'}
							backgroundColor={'#576423'}
							arrowDirection={'right'}
						/>
					</a>
				</Link>
			</div>
			<div className={styles.one_button}>
				<Button
					text={'рейтинг'}
					backgroundColor={'#696969'}
					arrowDirection={'right'}
				/>
			</div>
			<div className={styles.one_button}>
				<Link href={'./rooms'}>
					<a>
						<Button
							text={'відеокімната'}
							backgroundColor={'#46316E'}
							arrowDirection={'right'}
						/>
					</a>
				</Link>
			</div>
		</div>
		<div className={styles.brodcast_area}>
			<div className={styles.video}>
				<Image
					src="/img/videoMock.png"
					alt="video"
					width={885}
					height={500}
				/>
				<div className={styles.video_question}>
					<Image
						src="/img/video_question.png"
						alt="question"
						width={802}
						height={87}
						className={styles.video_question_decktop}
					/>
				</div>
			</div>

			<div className={styles.chat}>
				<Image
					src="/img/chat_mock.png"
					alt="chat"
					width={538}
					height={550}
				/>
			</div>
		</div>
	</MainLayout>
);

export default Main;
