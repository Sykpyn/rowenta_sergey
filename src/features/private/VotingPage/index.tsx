import React from 'react';
import MainLayout from '../../../layouts/MainLayout';
import PhotoGallery from '../../../ui/Components/PhotoGallery';

const RoomsPage = () => (
	<MainLayout>
		<PhotoGallery />
	</MainLayout>
);

export default RoomsPage;
