import Link from 'next/link';
import React from 'react';
import MainLayout from '../../../layouts/MainLayout';
import Button from '../../../ui/Components/Button';
import RoomsGallery from '../../../ui/Components/RoomsGallery';
import styles from './styles.module.css';

const RoomsPage = () => (
	<MainLayout>
		<div className={styles.go_to_main_button}>
			<Link href="/">
				<a>
					<Button text={'На головну'} />
				</a>
			</Link>
		</div>
		<div className={styles.title_block}>
			<h1 className={styles.title}>відеокімната</h1>
		</div>
		<RoomsGallery roomsAmount={15} />
	</MainLayout>
);

export default RoomsPage;
