import Link from 'next/link';
import React, { FC, useState } from 'react';
import MainLayout from '../../../layouts/MainLayout';
import Button from '../../../ui/Components/Button';
import Select from '../../../ui/Components/Select';
import TextInput from '../../../ui/Components/TextInput';
import styles from './style.module.css';
import CloseButton from '../../../ui/Components/CloseButton';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import { RegistrationSchema } from './types';
import SubmitButton from '../../../ui/Components/SubmitButton';

const data = [
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
	{ value: '123321' },
];

const RegisterForm = () => {
	const [open, setOpen] = useState(false);

	const onOpenModal = () => setOpen(true);
	const onCloseModal = () => setOpen(false);

	const { register, handleSubmit, errors } = useForm({
		resolver: yupResolver(RegistrationSchema),
	});

	const onSubmit = () => console.log(errors);

	return (
		<MainLayout>
			<div className={styles.go_to_main_button}>
				<Link href="/authorization">
					<a>
						<Button text={'Авторизація'} />
					</a>
				</Link>
			</div>
			<h1 className={styles.title}>Реєстрація</h1>
			<form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
				<div className={styles.fields_main}>
					<div className={styles.small_block_one}>
						<TextInput
							type={'text'}
							placeholder={`Ім'я`}
							refTo={register}
							name={'first_name'}
							error={errors.first_name}
						/>
						<TextInput
							type={'email'}
							placeholder={`Електронна пошта`}
							refTo={register}
							name={'email'}
							error={errors.email}
						/>{' '}
						<Select
							data={data}
							name={'team'}
							error={errors.team}
							refTo={register}
						></Select>
					</div>
					<div className={styles.small_block_two}>
						<TextInput
							type={'text'}
							placeholder={`Прізвище`}
							refTo={register}
							name={'last_name'}
							error={errors.last_name}
						/>
						<TextInput
							type={'password'}
							placeholder={`Пароль`}
							refTo={register}
							name={'password'}
							error={errors.password}
						/>
						<span className={styles.button}>
							<SubmitButton
								text={'зареєструватися'}
								arrowDirection={'right'}
								type="submit"
							/>
						</span>
					</div>
				</div>
				<div className={styles.checkbox_block}>
					<input
						type="checkbox"
						className={styles.custom_checkbox}
					></input>
					<div className={styles.checkbox_text}>
						<span className={styles.agreement}>
							Я даю згоду на обробку{' '}
						</span>
						<span
							className={styles.personal_data}
							onClick={() => onOpenModal()}
						>
							персональних даних
						</span>
					</div>
				</div>
			</form>
			<div className={styles.go_to_main_button_mobile}>
				<Link href="/authorization">
					<a>
						<Button text={'Авторизація'} />
					</a>
				</Link>
			</div>
			<Modal
				open={open}
				onClose={onCloseModal}
				center
				classNames={{
					modal: styles.modal_main,
				}}
				showCloseIcon={false}
			>
				<div className={styles.modal_text}>
					<h1>Політика конфіденційності</h1>
					<div className={styles.grey_line}></div>
					<div>
						<p>
							Ваша конфіденційність дуже важлива для нас. Ми
							хочемо, щоб Ваша робота в Інтернет по можливості
							була максимально приємною і корисною, і Ви абсолютно
							спокійно використовували найширший спектр
							інформації, інструментів і можливостей, які пропонує
							Інтернет.
						</p>
						<br />

						<p>
							Особиста інформація, зібрана при реєстрації (або в
							будь-який інший час) переважно використовується для
							підготовки Продуктів або Послуг відповідно до Ваших
							потреб. Ваша інформація не буде передана або продана
							третім сторонам. Однак ми можемо частково розкривати
							особисту інформацію в особливих випадках, описаних в
							«Згоду з розсилкою»
						</p>
						<br />
						<p>
							При добровільній реєстрації на отримання розсилки ви
							відправляєте своє Ім'я, E-mail і номер телефону
							через форму реєстрації.
						</p>
					</div>
					<div
						className={styles.close_btn}
						onClick={() => onCloseModal()}
					>
						<CloseButton />
					</div>
				</div>
			</Modal>
		</MainLayout>
	);
};

export default RegisterForm;
