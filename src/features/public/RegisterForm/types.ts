import * as yup from 'yup';

export const RegistrationSchema = yup.object().shape({
	first_name: yup.string().required('Введіть ваше ім’я'),
	last_name: yup.string().required('Введіть ваше прізвище'),
	email: yup
		.string()
		.email()
		.required('Будь ласка, введіть дійсну адресу електронної пошти'),
	password: yup.string().required('Введіть ваш пароль'),
	team: yup.string().required('Введіть ваше ім’я'),
});
