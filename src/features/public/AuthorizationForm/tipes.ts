import * as yup from 'yup';

export const AuthorizationSchema = yup.object().shape({
	email: yup.string().email().required('Это поля обязательное'),
	password: yup.string().required('Это поля обязательное'),
});
