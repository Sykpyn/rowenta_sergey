import Link from 'next/link';
import React, { FC } from 'react';
import MainLayout from '../../../layouts/MainLayout';
import Button from '../../../ui/Components/Button';
import TextInput from '../../../ui/Components/TextInput';
import styles from './style.module.css';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import SubmitButton from '../../../ui/Components/SubmitButton';
import { AuthorizationSchema } from './tipes';

const AuthorizationForm = () => {
	const { register, handleSubmit, errors } = useForm({
		resolver: yupResolver(AuthorizationSchema),
	});

	const onSubmit = () => console.log(errors);
	return (
		<MainLayout>
			<div className={styles.go_to_main_button}>
				<Link href="/register">
					<a>
						<Button text={'реєстрація'} />
					</a>
				</Link>
			</div>
			<div className={styles.authorization_main}>
				<div className={styles.red_line}></div>
				<h1 className={styles.title}>авторизація</h1>
				<form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
					<div className={styles.fields_main}>
						<TextInput
							type={'email'}
							placeholder={`Електронна пошта`}
							refTo={register}
							name={'email'}
							error={errors.email}
						/>
						<TextInput
							type={'password'}
							placeholder={`Пароль`}
							refTo={register}
							name={'password'}
							error={errors.password}
						/>
						<span className={styles.button}>
							<SubmitButton
								text={'Увійти'}
								arrowDirection={'right'}
								type="submit"
							/>
						</span>
					</div>
				</form>
			</div>
		</MainLayout>
	);
};

export default AuthorizationForm;
